module.exports = (app, express) => {

    const router = express.Router();
    const PropertyController = require('../controllers/PropertyController');
    const CF = require('../controllers/CommonFunctionController');
    const config = require("config");
    const baseApiUrl = config.get('baseApiUrl');
    var multer = require('multer');
    var path = require('path');

    var storage = multer.diskStorage({
        destination: function (req, file, cb) {

            // Uploads is the Upload_folder_name
            cb(null, "property_Image")
        },
        filename: function (req, file, cb) {
            cb(null, file.fieldname + "-" + Date.now() + ".jpg")
        }
    })

    const maxSize = 1 * 1000 * 1000;

    var upload = multer({
        storage: storage,
        limits: { fileSize: maxSize },
        fileFilter: function (req, file, cb) {

            // Set the filetypes, it is optional
            var filetypes = /jpeg|jpg|png/;
            var mimetype = filetypes.test(file.mimetype);

            var extname = filetypes.test(path.extname(
                file.originalname).toLowerCase());

            if (mimetype && extname) {
                return cb(null, true);
            }

            cb("Error: File upload only supports the "
                + "following filetypes - " + filetypes);
        }
    })

    router.post('/addProperty', upload.array('images', 6), (req, res, next) => {
        const propertyObj = (new PropertyController()).boot(req, res);
        return propertyObj.addProperty();
    });

    router.get('/properties', (req, res, next) => {
        const propertyObj = (new PropertyController()).boot(req, res);
        return propertyObj.getProperties();
    });

    router.put('/updateProperty/:id', (req, res, next) => {
        const propertyObj = (new PropertyController()).boot(req, res);
        return propertyObj.updateProperty();
    });

    app.use(config.baseApiUrl, router);
}