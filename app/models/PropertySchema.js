const mongoose = require('mongoose');
const schema = mongoose.Schema;

const imageSchema = mongoose.Schema({
    imageId : {type:schema.Types.ObjectId},
    url : String,
    thumbUrl : String,
});

const properties = new schema(
    {
        propertyName: {
            type: String,
        },
        images: [imageSchema],
        description: {
            type: String,
        },
        address: {
            type: String
        },
        area: {
            type: String
        },
        price: {
            type: String
        },
        bedroom: {
            type: String,
            enum: ["1", "2", "3", "4", "5"]
        },
        bathroom: {
            type: String,
            enum: ["1", "2", "3", "4", "5"]
        },
        carperArea: {
            type: String,
            enum: ["sq_ft", "sq_yd", "sq_mt"]
        },
        isFav: {
            type: Boolean,
            default: false
        },
        viewCounts: {
            type: Number,
            default: 0
        },
        recentlyViewed: {
            type: Date, 
            default: Date.now
        },
    },
    {
        timestamps: true
    }
);

const Properties = mongoose.model('properties', properties);

module.exports = {
    Properties
}