const Controller = require("./Controller");
const config = require("config");
const path = require('path');
const mv = require('mv');

class CommonFunctionController extends Controller {
    constructor() {
        super();
    }

    /*************************************************************************************
     Set Error Message
    **************************************************************************************/
    static getErrorMessage(status, message, code) {
        const res = {
            "settings": {
                "status": status,
                "message": message,
                "statusCode": code
            }
        }
        return res;
    }

    /*************************************************************************************
     Set Success Message
    **************************************************************************************/
    static getSuccessMessage(status, message, code, data) {
        const res = {
            "settings": {
                "status": status,
                "message": message,
                "statusCode": code
            },
            "data": data
        }
        return res;
    }
}
module.exports = CommonFunctionController;