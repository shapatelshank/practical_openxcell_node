
const Controller = require("./Controller");
const ObjectId = require('mongodb').ObjectID;
const { Properties } = require('../models/PropertySchema');
const CF = require('./CommonFunctionController');
const sharp = require('sharp');
var path = require('path');

class PropertyController extends Controller {
    constructor() {
        super();
    }

    async addProperty() {
        let _this = this;
        try {
            let images = [];
            let propertyDetails = {};
            images = _this.req.files;
            propertyDetails = _this.req.body;
            if (!propertyDetails.propertyName) { return _this.res.send(CF.getErrorMessage(0, 'Please provide property name', 400)); }
            if (!propertyDetails.description) { return _this.res.send(CF.getErrorMessage(0, 'Please provide description', 400)); }
            if (!propertyDetails.address) { return _this.res.send(CF.getErrorMessage(0, 'Please provide address', 400)); }
            if (!propertyDetails.area) { return _this.res.send(CF.getErrorMessage(0, 'Please provide area', 400)); }
            if (!propertyDetails.price) { return _this.res.send(CF.getErrorMessage(0, 'Please provide price', 400)); }
            if (!propertyDetails.bedroom) { return _this.res.send(CF.getErrorMessage(0, 'Please provide bedroom', 400)); }
            if (!propertyDetails.bathroom) { return _this.res.send(CF.getErrorMessage(0, 'Please provide bathroom', 400)); }
            if (!propertyDetails.carpetArea) { return _this.res.send(CF.getErrorMessage(0, 'Please provide carpet area', 400)); }
            const imageData = [];
            if (images.length > 0) {
                for (let index = 0; index < images.length; index++) {
                    const element = images[index];
                    await sharp(element.path, { failOnError: false })
                        .resize(64, 64)
                        .withMetadata()
                        .toFile(
                            path.resolve(element.destination, '', element.filename.replace(/\.(jpeg|jpg|png)$/, `_thumb.jpg`))
                        )
                    const image_url = '/property_Image/' + element.filename.replace(/\.(jpeg|jpg|png)$/, `.jpg`)
                    const thumb_url = '/property_Image/' + element.filename.replace(/\.(jpeg|jpg|png)$/, `_thumb.jpg`)
                    imageData.push({
                        url: image_url,
                        thumbUrl: thumb_url,
                    });
                }
            }
            propertyDetails['images'] = imageData;
            const propertyData = new Properties(propertyDetails);
            const addData = await propertyData.save();
            return _this.res.send(CF.getSuccessMessage(1, 'Congratulations ! Property has been added successfully', 200));
        }
        catch (error) {
            console.log("error", error);
            return _this.res.send(CF.getErrorMessage(0, "Internal Server Error", 500));
        }
    }

    async getProperties() {
        let _this = this;
        try {
            const limit = _this.req.query && _this.req.query.limit ? _this.req.query.limit : 10;
            const orderBy = _this.req.query && _this.req.query.orderBy ? _this.req.query.orderBy : "createdAt";
            let sortBy = {};
            sortBy[`${orderBy}`] = -1; 
            let getPropertyList = await Properties.find().sort(sortBy).limit(Number(limit));
            return _this.res.send(CF.getSuccessMessage(1, 'Property List get successfully', 200, getPropertyList));
        }
        catch (error) {
            console.log("error", error);
            return _this.res.send(CF.getErrorMessage(0, "Internal Server Error", 500));
        }
    }

    async updateProperty() {
        let _this = this;
        try {
            let propertyDetails = _this.req.body;
            let id = _this.req.params && _this.req.params.id;
            if (!id) { return _this.res.send(CF.getErrorMessage(0, 'Please provide property id', 400)); }
            let updateData = {};
            id = ObjectId(id);
            if(propertyDetails.isFav === true || propertyDetails.isFav === false){
                updateData = {
                    isFav: propertyDetails.isFav
                }
            } else if(propertyDetails.viewCounts > 0){
                updateData = {
                    viewCounts: propertyDetails.viewCounts,
                    recentlyViewed: Date.now()
                }
            } 
            const updatePropertyData = await Properties.updateOne({ _id : id }, updateData);
            return _this.res.send(CF.getSuccessMessage(1, 'Congratulations ! Property has been updated successfully', 200));
        }
        catch (error) {
            console.log("error", error);
            return _this.res.send(CF.getErrorMessage(0, "Internal Server Error", 500));
        }
    }
}
module.exports = PropertyController;